package com.example.ninoska.proyectoclinicasantamaria;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LogoutActivity extends AppCompatActivity {

    private EditText txtNombre, txtClave;
    private Button btnIngresar, btnCrearUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtClave = (EditText)findViewById(R.id.txtClave);
        btnIngresar = (Button)findViewById(R.id.btnIngresar);
        btnCrearUsuario = (Button)findViewById(R.id.btnCrearUsuario);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresarUsuario();
            }
        });


        btnCrearUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarUsuario();
            }
        });
    }

    private void ingresarUsuario() {
        String nombre = txtNombre.getText().toString();
        String clave = txtClave.getText().toString();
        if(nombre.equals("")) {
            Toast.makeText(this, "USUARIO INCORRECTO", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "USUARIO: " +nombre+ " CORRECTO", Toast.LENGTH_LONG).show();
            Intent in = new Intent(this, VistaActivity.class);
            startActivity(in);
        }
    }

    private void agregarUsuario() {
       Intent i = new Intent(this, CrearUsuarioActivity.class);
        startActivity(i);
        finish();
    }

}
