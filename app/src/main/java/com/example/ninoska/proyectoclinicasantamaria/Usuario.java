package com.example.ninoska.proyectoclinicasantamaria;

/**
 * Created by Ninoska on 29-05-2016.
 */
public class Usuario {
    private String nombre;
    private String apellido;
    private int edad;
    private String rut;
    private String profesion;
    private String areaTrabajo;
    private String nickUsuario;
    private String clave;
    private String reingreseClave;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getProfesion() {
        return profesion;
    }

    public void setProfesion(String profesion) {
        this.profesion = profesion;
    }

    public String getAreaTrabajo() {
        return areaTrabajo;
    }

    public void setAreaTrabajo(String areaTrabajo) {
        this.areaTrabajo = areaTrabajo;
    }

    public String getNickUsuario() {
        return nickUsuario;
    }

    public void setNickUsuario(String nickUsuario) {
        this.nickUsuario = nickUsuario;
    }

    public String getReingreseClave() {
        return reingreseClave;
    }

    public void setReingreseClave(String reingreseClave) {
        this.reingreseClave = reingreseClave;
    }
}
