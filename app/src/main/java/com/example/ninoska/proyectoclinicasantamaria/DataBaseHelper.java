package com.example.ninoska.proyectoclinicasantamaria;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ninoska.proyectoclinicasantamaria.Entidades.Persona;

import java.util.ArrayList;

/**
 * Created by Ninoska on 08-06-2016.
 */
public class DataBaseHelper extends SQLiteOpenHelper {
    private SQLiteDatabase db;
    public static final int VERSION_DB = 3;
    public static final String NAME_BD = "nombre.db";

    public static final String NOMBRE_TABLA_CONTACTO = "Persona";

    public static final String TC_KEY_ID = "id";
    public static final String TC_KEY_NICK = "nick";
    public static final String TC_KEY_RUT = "rut";
    public static final String TC_KEY_AREA = "area";
    public static final String TC_KEY_FECHA = "fecha";
    public static final String TC_KEY_HORARIOENTRADA = "horarioEntrada";
    public static final String TC_KEY_HORARIOSALIDA = "horarioSalida";

    public static final String TABLA_USUARIOS =
            "create table " + NOMBRE_TABLA_CONTACTO +
                    "( " + TC_KEY_ID + " text primary key autoincrement," +
                    TC_KEY_NICK + " text " +
                    TC_KEY_RUT + " text, " +
                    TC_KEY_AREA + " text, " +
                    TC_KEY_FECHA + " text, " +
                    TC_KEY_HORARIOENTRADA + " text, " +
                    TC_KEY_HORARIOSALIDA + " text) ";

    public DataBaseHelper(Context context) {
        super(context, NAME_BD, null, VERSION_DB);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DataBaseHelper.NAME_BD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + NOMBRE_TABLA_CONTACTO);
        onCreate(db);
        }


    public void insertar(String id, String nick, String rut, String area, String fecha, String horarioEntrada, String horarioSalida){
        ContentValues registro = new ContentValues();
        registro.put(DataBaseHelper.TC_KEY_ID, id);
        registro.put(DataBaseHelper.TC_KEY_NICK, nick);
        registro.put(DataBaseHelper.TC_KEY_RUT, rut);
        registro.put(DataBaseHelper.TC_KEY_AREA, area);
        registro.put(DataBaseHelper.TC_KEY_FECHA, fecha);
        registro.put(DataBaseHelper.TC_KEY_HORARIOENTRADA, horarioEntrada);
        registro.put(DataBaseHelper.TC_KEY_HORARIOSALIDA, horarioSalida);
        db.insert(DataBaseHelper.NOMBRE_TABLA_CONTACTO, null, registro);
    }

        ContentValues modifi = new ContentValues();
    public void modificar(String nick,String fechaNueva, String horarioEntradaNueva, String horarioSalidaNueva){
        modifi.put(TC_KEY_NICK, nick);
        modifi.put(TC_KEY_FECHA, fechaNueva);
        modifi.put(TC_KEY_HORARIOENTRADA, horarioEntradaNueva);
        modifi.put(TC_KEY_HORARIOSALIDA, horarioSalidaNueva);
        db.update(DataBaseHelper.NOMBRE_TABLA_CONTACTO, modifi,
                DataBaseHelper.TC_KEY_NICK + "=" + nick, null);
    }

    public Cursor leerDatos() {
        SQLiteDatabase db = getReadableDatabase();
        /**String query = ("SELECT * FROM " + NOMBRE_TABLA_CONTACTO +" WHERE 1 ORDER BY "+ TC_KEY_NICK + ";");
        Cursor c = db.rawQuery(query, null);

        if (c != null){
            c.moveToFirst();
        }
        return c;
         **/
        return db.rawQuery("SELECT id AS _id, nick AS rut FROM Persona;", null);
    }
    }
