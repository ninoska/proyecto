package com.example.ninoska.proyectoclinicasantamaria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.ninoska.proyectoclinicasantamaria.Entidades.Persona;

import java.util.ArrayList;
import java.util.List;

public class VistaActivity extends AppCompatActivity {
    private Button btnVerHorario, btnCambiarTurno, btnDesconectarse, btnIngresarHorario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista);

        btnCambiarTurno = (Button)findViewById(R.id.btnCambiarTurno);
        btnVerHorario = (Button)findViewById(R.id.btnVerHorario);
        btnDesconectarse = (Button)findViewById(R.id.btnDesconectarse);
        btnIngresarHorario = (Button)findViewById(R.id.btnIngresarHorario);

        btnIngresarHorario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingresoHorario();
            }
        });
        btnVerHorario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verHorarios();
            }
        });

        btnDesconectarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                desconectarse();

            }
        });
        btnCambiarTurno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarHorario();
            }
        });
    }

    private void cambiarHorario() {
        Intent id = new Intent(this, CambiarTurno.class);
        startActivity(id);
    }

    private void ingresoHorario() {
        Intent i = new Intent(this, HorarioActivity.class);
        startActivity(i);
    }

    private void desconectarse() {
        Intent in = new Intent(this, LogoutActivity.class);
        startActivity(in);
        finish();
    }

    private void verHorarios() {
        Intent inte = new Intent(this, ListarActivity.class);
        startActivity(inte);
    }
}
