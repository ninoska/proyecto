package com.example.ninoska.proyectoclinicasantamaria;

import com.example.ninoska.proyectoclinicasantamaria.Entidades.Persona;

import java.util.ArrayList;
/**
 * Created by Ninoska on 29-05-2016.
 */
public class BaseDeDatos {
    private static ArrayList<Persona>values = new ArrayList<>();

    public static void agregarUsuario(Persona persona){
        values.add(persona);
    }

    public static ArrayList<Persona>getValues(){
        return values;
    }
}
