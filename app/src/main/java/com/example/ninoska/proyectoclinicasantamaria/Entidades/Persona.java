package com.example.ninoska.proyectoclinicasantamaria.Entidades;

/**
 * Created by Ninoska on 06-06-2016.
 */
    import com.google.gson.annotations.SerializedName;

public class Persona {

    @SerializedName("id")
    private String id;
    @SerializedName("nick")
        private String nick;
        @SerializedName("rut")
        private String rut;
        @SerializedName("area")
        private String area;
        @SerializedName("fecha")
        private String fecha;
        @SerializedName("horarioEntrada")
        private String horarioEntrada;
        @SerializedName("horarioSalida")
        private String horarioSalida;

    public Persona() {
    }

    public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick= nick;
        }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getHorarioEntrada() {
        return horarioEntrada;
    }

    public void setHorarioEntrada(String horarioEntrada) {
        this.horarioEntrada = horarioEntrada;
    }

    public String getHorarioSalida() {
        return horarioSalida;
    }

    public void setHorarioSalida(String horarioSalida) {
        this.horarioSalida = horarioSalida;
    }


    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}












