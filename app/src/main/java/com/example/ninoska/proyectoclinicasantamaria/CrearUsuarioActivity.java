package com.example.ninoska.proyectoclinicasantamaria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

public class CrearUsuarioActivity extends AppCompatActivity {

    private EditText txtNombreUsuario, txtApellidoUsuario, txtEdad, txtRutUsuario,
            txtProfesion, txtAreaUsuario, txtUsuario, txtClaveUsuario, txtRepClave;
    private Button btnGrabar, btnVolver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_usuario);

        btnVolver = (Button)findViewById(R.id.btnVolver);
        btnGrabar = (Button)findViewById(R.id.btnGrabar);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresarPantalla();
            }
        });
        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grabarUsuario();
            }
        });

        txtNombreUsuario = (EditText)findViewById(R.id.txtNombreUsuario);
        txtApellidoUsuario = (EditText)findViewById(R.id.txtApellidoUsuario);
        txtEdad = (EditText)findViewById(R.id.txtEdad);
        txtRutUsuario = (EditText)findViewById(R.id.txtRutUsuario);
        txtProfesion = (EditText)findViewById(R.id.txtProfesion);
        txtAreaUsuario = (EditText)findViewById(R.id.txtAreaUsuario);
        txtUsuario = (EditText)findViewById(R.id.txtUsuario);
        txtClaveUsuario = (EditText)findViewById(R.id.txtClaveUsuario);
        txtRepClave = (EditText)findViewById(R.id.txtRepClave);
        cargarDatos();
    }

    private void regresarPantalla() {
        Intent i = new Intent(this, LogoutActivity.class);
        startActivity(i);
    }

    private void cargarDatos(){
    Usuario usuario = SesionPreferences.getInstance().obtenerUsuario(this);
        if(usuario != null){
            txtNombreUsuario.setText(usuario.getNombre());
            txtApellidoUsuario.setText(usuario.getApellido());
            txtEdad.setText(usuario.getEdad()+ "");
            txtRutUsuario.setText(usuario.getRut());
            txtProfesion.setText(usuario.getProfesion());
            txtAreaUsuario.setText(usuario.getAreaTrabajo());
            txtUsuario.setText(usuario.getNickUsuario());
            txtClaveUsuario.setText(usuario.getClave());
            txtRepClave.setText(usuario.getReingreseClave());
        }
    }

    private void grabarUsuario() {
        Usuario usuario = new Usuario();
        usuario.setNombre(txtNombreUsuario.getText().toString());
        usuario.setApellido(txtApellidoUsuario.getText().toString());
        usuario.setEdad(Integer.parseInt(txtEdad.getText().toString()));
        usuario.setRut(txtRutUsuario.getText().toString());
        usuario.setProfesion(txtProfesion.getText().toString());
        usuario.setAreaTrabajo(txtAreaUsuario.getText().toString());
        usuario.setNickUsuario(txtUsuario.getText().toString());
        usuario.setClave(txtClaveUsuario.getText().toString());
        usuario.setReingreseClave(txtRepClave.getText().toString());

        Gson gson = new Gson();
        String usuarioJson = gson.toJson(usuario);
        SesionPreferences.getInstance().guardarUsuario(this, usuarioJson);

        Toast.makeText(this, "USUARIO GUARDADO", Toast.LENGTH_LONG).show();

    }
}
