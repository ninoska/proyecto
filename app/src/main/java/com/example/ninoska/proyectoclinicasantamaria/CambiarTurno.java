package com.example.ninoska.proyectoclinicasantamaria;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class CambiarTurno extends AppCompatActivity {

    private EditText txtNickUsuario, txtFechaHorario, txtHorarioEntradaAct, txtHorarioSalidaAct;
    private Button btnActualizar, btnVolver;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_turno);

        txtNickUsuario = (EditText) findViewById(R.id.txtNickUsuario);
        txtFechaHorario = (EditText) findViewById(R.id.txtFechaHorario);
        txtHorarioEntradaAct = (EditText) findViewById(R.id.txtHorarioEntradaAct);
        txtHorarioSalidaAct = (EditText) findViewById(R.id.txtHorarioSalidaAct);
        btnActualizar = (Button) findViewById(R.id.btnActualizar);
        btnVolver = (Button) findViewById(R.id.btnVolver);

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actulizarHorario();
            }
        });

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void actulizarHorario() {
        String nick = txtNickUsuario.getText().toString();
        String fecha = txtFechaHorario.getText().toString();
        String horarioEntrada = txtHorarioEntradaAct.getText().toString();
        String horarioSalida = txtHorarioSalidaAct.getText().toString();
        DataBaseHelper baseHelper = new DataBaseHelper(this);
        SQLiteDatabase db = baseHelper.getWritableDatabase();

        if (db != null) {
            ContentValues registro = new ContentValues();
            registro.put("Nick", nick);
            registro.put("Fecha", fecha);
            registro.put("HorarioEntrada", horarioEntrada);
            registro.put("HorarioSalida", horarioSalida);
            db.update(DataBaseHelper.NOMBRE_TABLA_CONTACTO, registro, "nick='" + nick + "'" , null);

            if(nick.equals("")) {
                Toast.makeText(this, nick +" NO Actualizado Correctamente", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(this, nick + " Actualizado Correctamente", Toast.LENGTH_SHORT).show();
            }

        }
        }

    private void regresar() {
        Intent in = new Intent(this, VistaActivity.class);
        startActivity(in);
        finish();
    }


    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "CambiarTurno Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.ninoska.proyectoclinicasantamaria/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "CambiarTurno Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.ninoska.proyectoclinicasantamaria/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
