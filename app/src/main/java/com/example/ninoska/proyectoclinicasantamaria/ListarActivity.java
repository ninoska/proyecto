package com.example.ninoska.proyectoclinicasantamaria;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

public class ListarActivity extends AppCompatActivity {

    private DataBaseHelper db;
    private ListView lista;
    private Cursor cursor;
    List<String> item = null;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        lista = (ListView) findViewById(R.id.lista);
        cargar();
    }

    public void cargar() {
        DataBaseHelper baseHelper = new DataBaseHelper(this);
        SQLiteDatabase db = baseHelper.getReadableDatabase();
        if (db != null) {
            Cursor c = db.rawQuery("select * from Persona", null);
            int cantidad = c.getCount();
            int i = 0;
            String[] arreglo = new String[cantidad];
            if (c.moveToFirst()) {
                do {
                    String linea = c.getString(0) + "" + c.getString(1) + "" + c.getString(2) + "" +
                            c.getString(3) + "" + c.getString(4) + "" + c.getString(5) + "" + c.getString(6);

                    arreglo[i] = linea;
                    i++;
                } while (c.moveToNext());
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.activity_listar, arreglo);
            ListView lista = (ListView) findViewById(R.id.lista);
            lista.setAdapter(adapter);
        }
    }
}

