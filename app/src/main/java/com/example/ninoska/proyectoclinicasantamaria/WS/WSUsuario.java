package com.example.ninoska.proyectoclinicasantamaria.WS;

import com.example.ninoska.proyectoclinicasantamaria.Entidades.Persona;
import com.example.ninoska.proyectoclinicasantamaria.Usuario;
import com.google.gson.Gson;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ninoska on 01-06-2016.
 */
public class WSUsuario {

    private static final long TIME_OUT_SECONDS = 5000;
    private static WSUsuario instance;

    public static WSUsuario getInstance() {
        if (instance == null) {
            instance = new WSUsuario();
        }
        return instance;
    }

    private WSUsuario(){}

    public Persona[] listadoUsuario(){
        Persona[] values = null;

        String url = "https://script.google.com/macros/s/AKfycbyeBuIwUd2uMgz1oXqxlZG_buwS3hFv-lko0vvf2iunDkcQBKfN/exec";
        try {
            String respuesta = getRequest(url);
            Gson gson = new Gson();
            values = gson.fromJson(respuesta, Persona[].class);

        } catch (Exception e) {

        }

        return values;
    }

    private String postRequest(String url, FormEncodingBuilder params) throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);

        RequestBody formBody = params.build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }

    private String getRequest(String url)  throws IOException {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS); // connect timeout
        client.setReadTimeout(TIME_OUT_SECONDS, TimeUnit.SECONDS);
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("cache-control", "no-cache")
                .build();

        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        return respuesta;
    }
}
