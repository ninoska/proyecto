package com.example.ninoska.proyectoclinicasantamaria;

/**
 * Created by Ninoska on 29-05-2016.
 */

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;


public class SesionPreferences {

    private static final String NOMBRE_SHARED = "sesion_usuario";
    private static final String KEY_NOMBRE = "key_nombre";
    private static final String KEY_APELLIDO = "key_apellido";
    private static final String KEY_EDAD = "key_edad";
    private static final String KEY_RUT = "key_rut";
    private static final String KEY_PROFESION = "key_profesion";
    private static final String KEY_AREA = "key_area";
    private static final String KEY_USUARIO = "key_usuario";
    private static final String KEY_CLAVE = "key_clave";
    private static final String KEY_REPCLAVE = "key_repClave";

    public static final String KEY_USUARIO_JSON = "key_usuario_json";

    private static SesionPreferences instance;

    private SesionPreferences(){}

    public static SesionPreferences getInstance(){

        if(instance == null){
            instance = new SesionPreferences();
        }
        return instance;
    }

    public void guardarSesion(Context context, String nombre, String apellido, int edad, String rut, String profesion, String area, String usuario, String clave, String repClave) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_NOMBRE, nombre);
        edit.putString(KEY_APELLIDO, apellido);
        edit.putInt(KEY_EDAD, edad);
        edit.putString(KEY_RUT, rut);
        edit.putString(KEY_PROFESION, profesion);
        edit.putString(KEY_AREA, area);
        edit.putString(KEY_USUARIO, usuario);
        edit.putString(KEY_CLAVE, clave);
        edit.putString(KEY_REPCLAVE, repClave);
        edit.commit();
    }

    public void guardarUsuario(Context context, String usuarioJson) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_USUARIO_JSON, usuarioJson);
        edit.commit();
    }

    public void cerrarSesion(Context context) {
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = sha.edit();
        edit.putString(KEY_NOMBRE, "");
        edit.commit();
    }

    public Usuario obtenerUsuario(Context context) {
        Usuario usuario = null;
        SharedPreferences sha = context.getSharedPreferences(NOMBRE_SHARED, Context.MODE_PRIVATE);
        try {
            String usuarioJson = sha.getString(KEY_USUARIO_JSON, "");
            Gson gson = new Gson();
            usuario = gson.fromJson(usuarioJson, Usuario.class);
        }catch (Exception e){
        }

        return usuario;
    }

}
