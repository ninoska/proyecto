package com.example.ninoska.proyectoclinicasantamaria;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.MessageQueue;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ninoska.proyectoclinicasantamaria.Entidades.Persona;
import com.example.ninoska.proyectoclinicasantamaria.DataBaseHelper;

public class HorarioActivity extends AppCompatActivity {

    private EditText txtId, txtNick, txtRut, txtArea, txtFecha, txtHorarioEntrada, txtHorarioSalida;
    private Button btnGrabar, btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario);

        txtId = (EditText)findViewById(R.id.txtId);
        txtNick = (EditText)findViewById(R.id.txtNick);
        txtRut = (EditText)findViewById(R.id.txtRut);
        txtArea = (EditText)findViewById(R.id.txtArea);
        txtFecha = (EditText)findViewById(R.id.txtFecha);
        txtHorarioEntrada = (EditText)findViewById(R.id.txtHorarioEntrada);
        txtHorarioSalida = (EditText)findViewById(R.id.txtHorarioSalida);
        btnGrabar = (Button)findViewById(R.id.btnGrabar);
        btnVolver = (Button)findViewById(R.id.btnVolver);



        btnGrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grabarHorario();
            }
        });
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresarHorario();
            }
        });
    }

    private void grabarHorario() {
        String id = txtId.getText().toString();
        String nick = txtNick.getText().toString();
        String rut = txtRut.getText().toString();
        String area = txtArea.getText().toString();
        String fecha = txtFecha.getText().toString();
        String horarioEntrada = txtHorarioEntrada.getText().toString();
        String horarioSalida = txtHorarioSalida.getText().toString();

        DataBaseHelper baseHelper = new DataBaseHelper(this);
        SQLiteDatabase db = baseHelper.getWritableDatabase();

        if (db !=null) {
            ContentValues registro = new ContentValues();
            registro.put("Id", id);
            registro.put("Nick", nick);
            registro.put("Rut", rut);
            registro.put("Area", area);
            registro.put("Fecha", fecha);
            registro.put("HorarioEntrada", horarioEntrada);
            registro.put("HorarioSalida", horarioSalida);
           // baseHelper.insertar(id, nick, rut, area, fecha, horarioEntrada, horarioSalida);
            db.insert(DataBaseHelper.NOMBRE_TABLA_CONTACTO, null, registro);

            if(nick != null) {
                Toast.makeText(this, "Horario de "+ nick +" Insertado Correctamente", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(this, "Horario de "+ nick +" NO Insertado Correctamente", Toast.LENGTH_SHORT).show();
            }
            }
        }




    private void regresarHorario() {
        Intent in = new Intent(this, VistaActivity.class);
        startActivity(in);
        finish();
    }
}
